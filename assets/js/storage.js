export default {
    STORAGE_NAME: "history",
    history: [],
    init: function () {
        if (localStorage.getItem(this.STORAGE_NAME)) {
            this.history = JSON.parse(localStorage.getItem(this.STORAGE_NAME));
        } else {
            this.history = [];
            localStorage.setItem(this.STORAGE_NAME, JSON.stringify(this.history));
        }
    },
    sync: function () {
        localStorage.setItem(this.STORAGE_NAME, JSON.stringify(this.history));
    },
    add: function (name, playerScore, computerScore) {
        this.history.push({
            name,
            player: playerScore,
            computer: computerScore,
            time: new Date()
        })
        this.sync();

    },
}


