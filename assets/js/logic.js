var arrayRockScissorPaper = ["rock", "scissor", "paper"];
// 0 rock, 1 scissor, 2 paper

function getResult(player, computer) {
    let result;
    // hasil -1 kalah, 0 seri, 1 menang

    if (player === computer) {
        result = 0;
    } else {
        switch (player) {
            case arrayRockScissorPaper[0]:
                result = (computer == arrayRockScissorPaper[1]) ? 1 : -1;
                break;
            case arrayRockScissorPaper[1]:
                result = (computer == arrayRockScissorPaper[2]) ? 1 : -1;
                break
            case arrayRockScissorPaper[2]:
                result = (computer == arrayRockScissorPaper[0]) ? 1 : -1;
                break
        }
    }

    return result;
}

// console.log(getResult("rock","paper"));

function getComputerChoose() {
    return arrayRockScissorPaper[Math.floor(Math.random() * arrayRockScissorPaper.length)];
}

export { getResult, getComputerChoose, arrayRockScissorPaper };

// console.log(getResult("rock",getComputerChoose()));