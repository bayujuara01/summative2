import { getResult, getComputerChoose, arrayRockScissorPaper } from "./logic.js";
import Storage from "./storage.js";

// var
let isClicked = true;
const audio = new Audio("./assets/sound/started.m4a");
const Game = {
    name: "Guest",
    player: 0,
    computer: 0,
    round: 0,
    maxRound: 0
}

// Variable DOM
const computerSelector = document.querySelector(".computer");
const playerSelectors = document.querySelector("#selector");
const resultContentCondition = document.querySelector(".content-result");
const playerScore = document.querySelector("#player-score");
const computerScore = document.querySelector("#computer-score");
const btnSubmit = document.querySelector("#btn-submit");
const inputName = document.querySelector("#input-name");
const inputRound = document.querySelector("#input-round");
const modalContainer = document.querySelector("#init");
const winContainer = document.querySelector("#win");
const loseContainer = document.querySelector("#lose");
const btnReloadWin = document.querySelector("#btn-reload-win");
const btnReloadLose = document.querySelector("#btn-reload-lose");
const histories = document.querySelector("#history-data");

// Init
document.addEventListener('DOMContentLoaded', () => {
    // Storage.init();
    // histories.innerHTML = Storage.history.map(game => {
    //     return `
    //         <tr>
    //         <td>${game.name}</td>
    //         <td>${game.player > game.computer ? "WIN" : "LOSE"}</td>
    //         </tr>
    //     `;
    // }).join(" ");
});

const computerRandomImage = (element, result, milisecond = 1000) => new Promise((resolve, reject) => {
    const initTime = new Date().getTime();
    let index = 0;
    const duration = setInterval(() => {
        const currentTime = new Date().getTime();
        element.setAttribute("src", `./assets/images/${arrayRockScissorPaper[index++]}.png`)
        if (currentTime - initTime > milisecond) {
            clearInterval(duration);
            resolve(result)
            // console.log(currentTime - initTime);
        }
        if (index > 2) {
            index = 0;
        }
    }, 100);

});

function getWinningCondition(condition) {
    let result;
    switch (condition) {
        case -1:
            result = "LOSE";
            break;
        case 1:
            result = "WIN";
            break;
        default:
            result = "DRAW";
            break;
    }
    return result;
}

function score(status) {
    if (status === 1) {
        Game.player += 1;
    } else if (status === -1) {
        Game.computer += 1;
    }

    playerScore.innerHTML = Game.player;
    computerScore.innerHTML = Game.computer;
    // console.log(`status: ${status}, player: ${Game.player}, computer:${Game.computer}`);
}

playerSelectors.addEventListener("click", (event) => {
    if (!isClicked) {
        return;
    }
    isClicked = false;
    if (Game.round === 0)
        return;
    audio.play();
    resultContentCondition.innerHTML = "<h1 class=\"result\">-</h1>";
    if (event.target.className == "player") {
        const computerChoose = getComputerChoose();
        const playerChoose = event.target.getAttribute('data');
        const status = getResult(playerChoose, computerChoose);


        computerRandomImage(computerSelector, computerChoose, 1250)
            .then((result) => {
                // console.log(`Player Choose : ${playerChoose},Computer Choose : ${computerChoose}`);
                computerSelector.setAttribute("src", `./assets/images/${result}.png`);
                resultContentCondition.innerHTML = `<h1 class="result">${getWinningCondition(status)}</h1>`;
                score(status);

                if (status != 0) {
                    Game.round--;
                }

                if (Game.maxRound % 2 == 0) {
                    if ((Game.player > (Game.maxRound / 2)) || (Game.computer > (Game.maxRound / 2))) {
                        console.log(Game);
                        Game.round = 0;
                    }
                } else {
                    if ((Game.player >= Math.ceil((Game.maxRound) / 2)) || (Game.computer >= Math.ceil((Game.maxRound) / 2))) {
                        console.log(Game);
                        Game.round = 0;
                    }
                }

                if (Game.round <= 0) {
                    if (Game.player > Game.computer) {
                        winContainer.setAttribute("style", "display:block");
                    } else {
                        loseContainer.setAttribute("style", "display:block");
                    }
                    Storage.add(Game.name, Game.player, Game.computer);
                }

                isClicked = true;
            });
    }
});

btnSubmit.addEventListener("click", () => {
    modalContainer.setAttribute("style", "display:none");
    let name = inputName.value;
    let round = inputRound.value;
    if (round < 3) round = inputRound.getAttribute("default-value");
    Game.name = name;
    Game.round = round;
    Game.maxRound = round;
    console.log("Round : " + Game.round);
});

btnReloadWin.addEventListener("click", () => {
    window.location.replace("/")
});

btnReloadLose.addEventListener("click", () => {
    window.location.replace("/")
});