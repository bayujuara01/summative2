# summative2

### Nexsoft Fun Coding Bootcamp Batch 7 Summative 2

## Author
### Bayu Seno Ariefyanto & Akbar Widyoseno

## Preview Image For Rock-Scissors-Paper
![Main Menu](game-1-menu.png)

![Game Start](game-2-started.png)

![Lose Condition](game-3-lose.png)

![Win Condition](game-4-win.png)
